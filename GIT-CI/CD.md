Установка GitLub-Runner



```shell
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
```
# где будет в строке вопрос executor выбираем shell

Запуск ранера
```shell
gitlab-runner register  --url https://gitlab.com  --token glrt-9FHCuDxxj_4iVKRfEMLe
gitlab-runner start
```



Установка Docker
# Обновление системы
```shell
sudo apt update
sudo apt upgrade -y
```


# Установка необходимых пакетов для поддержки HTTPS
```shell
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
```


# Добавление официального ключа Docker GPG
```shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

```

# Добавление официального репозитория Docker
```shell
echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

```

# Установка Docker Engine
```shell
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
```


# Добавление пользователя в группу docker
```shell
sudo usermod -aG docker $USER
```


# Выход из системы и вход заново, чтобы изменения вступили в силу

# Проверка установки Docker
```shell
docker --version
```


# Установка Docker Compose

# Обновление системы
```shell
sudo apt update
sudo apt upgrade -y
```


# Установка необходимых пакетов для поддержки HTTPS
```shell
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
```


# Добавление официального ключа Docker GPG
```shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

# Добавление официального репозитория Docker
````shell
echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
````

# Установка Docker Engine
````shell
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
````

# Добавление пользователя в группу docker
```shell
sudo usermod -aG docker $USER
```


# Выход из системы и вход заново, чтобы изменения вступили в силу

# Проверка установки Docker
```shell
docker --version
```

